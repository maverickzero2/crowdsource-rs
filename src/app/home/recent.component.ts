import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { UtilService } from '../util.service';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

interface RecentRsData {
  placeId: string;
  name: string;
  longAddress: string;
  shortAddress: string;
  neededItemsCount: number;
  urgentItemsCount: number;
  distance: number;
  lastUpdateTs: number;
  updatedAgo: string;
}

@Component({
  selector: 'app-list-recent',
  templateUrl: './recent.component.html',
  styleUrls: ['./recent.component.css']
})
export class RecentComponent {
  static LIMIT_RECENT_RS = 8;

  recentRs$: Observable<RecentRsData[]>;
  activatingLocation = false;
  Date = Date;

  constructor(
    public utilService: UtilService,
    public userService: UserService
  ) {
    this.recentRs$ =
      utilService.allPlaceData$.pipe(
        map(rsData => rsData
          .map<RecentRsData>(rs => {
            return {
              placeId: rs.placeData.place.place_id,
              name: rs.placeData.place.name,
              longAddress: rs.placeData.place.formatted_address,
              shortAddress: this.buildShortAddress(rs.placeData.place.address_components),
              neededItemsCount: rs.totalKebutuhan,
              urgentItemsCount: rs.totalMendesak,
              distance: rs.distanceToUser,
              lastUpdateTs: rs.lastUpdateTs,
              updatedAgo: utilService.timeAgo(rs.lastUpdateTs)
            };
          })
          .sort((a, b) => {
            // Sort by nearest distance and last update timestamp.
            if (a.distance < b.distance) return -1;
            if (a.distance > b.distance) return 1;
            return b.lastUpdateTs - a.lastUpdateTs;
          })
          .slice(0, RecentComponent.LIMIT_RECENT_RS)
        ),
        shareReplay(1)
      );
  }

  activateLocation(enable_location: boolean) {
    this.activatingLocation = true;
    return this.userService.post('enable_location', { enable_location })
      .then(() => this.activatingLocation = false);
  }

  private buildShortAddress(address_components): string {
    const provinsi = address_components.filter(addr => addr.types.indexOf('administrative_area_level_1') > -1)[0]?.['short_name'];
    const kabupaten = address_components.filter(addr => addr.types.indexOf('administrative_area_level_2') > -1)[0]?.['short_name'];
    const kecamatan = address_components.filter(addr => addr.types.indexOf('administrative_area_level_3') > -1)[0]?.['short_name'];
    return `${kecamatan}, ${kabupaten}, ${provinsi}`;
  }
}
