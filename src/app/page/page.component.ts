import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, filter, tap } from 'rxjs/operators';

export const pages = [
  { slug: 'about', title: 'Tentang Kami' },
  { slug: 'privacy', title: 'Kebijakan Privasi' },
  { slug: 'faq', title: 'FAQ' },
  { slug: 'roles', title: 'Peran' },
  { slug: 'disclaimer', title: 'Sanggahan' },
];

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {
  slug$: Observable<string>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.slug$ = this.route.paramMap.pipe(
      map((params: ParamMap) => params.get('slug')),
      filter<string>(Boolean),
      tap((slug) => {
        // Redirect to home if slug 404
        if (pages.map(p => p.slug).indexOf(slug) == -1) {
          this.router.navigate(['/home']);
        }
      })
    )
  }

  ngOnInit(): void {
  }

}
