import { Component, OnDestroy } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap, bufferCount, shareReplay } from 'rxjs/operators';
import { Observable, Subject, Subscription } from 'rxjs';
import { UserService } from '../user.service';
import { UtilService } from '../util.service';
import { Attributes } from '../rs/rs.component';


@Component({
  selector: 'app-viz',
  templateUrl: './viz.component.html',
  styleUrls: ['./viz.component.css']
})

export class VizComponent implements OnDestroy {
  location$: Observable<any>;
  attributes = Attributes;
  displayOption = 'gridView';
  handsetChartClicked = new Subject<any>();
  handsetChartClicked$ = this.handsetChartClicked.asObservable();
  handsetChartClickedSub: Subscription;

  constructor(
    public userService: UserService,
    public utilService: UtilService,
    private firestore: AngularFirestore,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.location$ = this.route.paramMap.pipe(
      map(params => params.get('id') || '0:Indonesia'),
      switchMap((id: string) =>
        this.firestore.collection<any>('hierarchy')
          .doc(id).valueChanges().pipe(map((loc: any) => {
            const children = [];
            if (id.startsWith('4:')) {
              for (let [rs_id, poi] of Object.entries<any>(loc.children)) {
                let logisticDemands = [];
                for (let l of this.attributes.logistic) {
                  if (poi.data[l.key] && poi.data[l.key] > 0) logisticDemands.push({ name: l.name, count: poi.data[l.key], unitLabel: l.unitLabel });
                }
                children.push({
                  link: ['/rs', poi.place.place_id],
                  name: poi.place.name,
                  data: poi.data,
                  logisticDemands: logisticDemands
                });
              }
            } else {
              for (let [name, data] of Object.entries(loc.children)) {
                let logisticDemands = [];
                for (let l of this.attributes.logistic) {
                  if (data[l.key] && data[l.key] > 0) logisticDemands.push({ name: l.name, count: data[l.key], unitLabel: l.unitLabel });
                }
                children.push({ link: ['/viz', name], name, data, logisticDemands: logisticDemands });
              }
            }

            loc.children = children
              .map(({ link, name, data, logisticDemands }) => {
                return { link, name, data, filledLogistic: logisticDemands.length }
              })
              .sort((a, b) => a.name < b.name ? -1 : 1);

            const filledLogistic = children
              .map(({ link, name, logisticDemands }) => {
                return { link, name, logisticDemands, totalLogisticDemand: logisticDemands.reduce((total, num) => total + num.count, 0) };
              })
              .sort((a, b) => a.totalLogisticDemand < b.totalLogisticDemand ? 1 : -1);

            loc.chart = {
              dataSets: [
                {
                  label: 'Jumlah Kebutuhan',
                  data: filledLogistic.map(x => x.totalLogisticDemand),
                  links: filledLogistic.map(x => x.link),
                  tooltipsData: filledLogistic.map(x => x.logisticDemands)
                }
              ],
              labels: filledLogistic.map(x => {
                return x.name.indexOf(':') > -1 ? x.name.split(':')[1] : x.name;
              })
            };
            return loc;
          }))),
      shareReplay(1)
    );
    // Set default view template based on device type
    // if (utilService.isHandset) this.displayOption = 'listView';
    // else this.displayOption = 'chartView';

    this.handsetChartClickedSub = this.handsetChartClicked$
      .pipe(bufferCount(2))
      .subscribe(x => {
        this.router.navigate(x[0].link);
      });
  }
  chartClicked($event) {
    if ($event.dataSets.links && $event.selectedIndex > -1) {
      if (this.utilService.isHandset) {
        this.handsetChartClicked.next({ label: $event.label, link: $event.dataSets.links[$event.selectedIndex] });
      } else {
        this.router.navigate($event.dataSets.links[$event.selectedIndex]);
      }
    }
  }
  ngOnDestroy() {
    if (this.handsetChartClickedSub) this.handsetChartClickedSub.unsubscribe();
  }
}
