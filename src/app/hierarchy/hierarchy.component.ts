import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, combineLatest } from 'rxjs';
import { HierarchyData, ITEM_STATUS, AggData, Attributes, RsData, toRsData, toPlaceData } from '../rs/rs.component';
import { ActivatedRoute } from '@angular/router';
import { map, shareReplay, switchMap, startWith } from 'rxjs/operators';
import { UtilService } from '../util.service';
import { UserService } from '../user.service';

interface AggregatedData {
  name: string;
  data: AggData;
}

interface ChildHierarchyData {
  id: number;
  name: string;
  numFaskes: number;

  suspects: AggregatedData[];
  logistics: AggregatedData[];
  beds: AggregatedData[];
  availabilities: AggregatedData[];
}

@Component({
  selector: 'app-hierarchy',
  templateUrl: './hierarchy.component.html',
})
export class HierarchyComponent {
  h$: Observable<HierarchyData>;
  childrenHierarchy$: Observable<ChildHierarchyData[]>;
  childrenRsData$: Observable<RsData[]>;
  ITEM_STATUS = ITEM_STATUS;

  constructor(
    public utilService: UtilService,
    private route: ActivatedRoute,
    private userService: UserService,
    firestore: AngularFirestore,
  ) {
    const groupKey = {}, shortLabel = {}, unitLabel = {};
    for (let a of Attributes.suspect) {
      groupKey[a.key] = 'suspects';
      shortLabel[a.key] = a.name || a.shortLabel;
    }
    for (let a of Attributes.logistic) {
      groupKey[a.key] = 'logistics';
      shortLabel[a.key] = a.name || a.shortLabel;
      unitLabel[a.key] = a.unitLabel;
    }
    for (let a of Attributes.bed) {
      groupKey[a.key] = 'beds';
      shortLabel[a.key] = a.name || a.shortLabel;
    }
    for (let a of Attributes.availibility) {
      groupKey[a.key] = 'availabilities';
      shortLabel[a.key] = a.name || a.shortLabel;
      unitLabel[a.key] = a.unitLabel;
    }

    this.h$ =
      this.route.paramMap.pipe(
        map(params => params.get('id')),
        switchMap(id => firestore.collection('h').doc<HierarchyData>(id).valueChanges()),
        shareReplay(1)
      );
    this.childrenHierarchy$ = this.h$.pipe(map(h => {
      if (!h?.childHieData) return [];
      return Object.entries(h.childHieData)
        .map(([cid, hieData]) => {
          const c: ChildHierarchyData = {
            id: +cid,
            name: h.kpData.children[cid],
            numFaskes: hieData.agg.count,
            suspects: [],
            logistics: [],
            beds: [],
            availabilities: [],
          };
          for (const [key, data] of Object.entries(hieData.agg)) {
            if (key === 'count') continue;
            const arr = c[groupKey[key]] as AggregatedData[];
            if (!arr) {
              console.error('Key not found: ', key);
              continue;
            }
            const sum = Object.values<number>(data?.sumByStatus).reduce((prev, cur) => prev + cur, 0);
            if (sum === 0) continue;
            arr.push({ name: shortLabel[key], data });
          }
          return c;
        })
        .sort((a, b) => {
          return b.numFaskes - a.numFaskes;
        });
    }));

    this.childrenRsData$ =
      combineLatest(
        this.h$.pipe(map(h => Object.values(h?.rsData || {}))),
        this.userService.user$.pipe(startWith(null))
      ).pipe(
        map(([arr, user]) => toRsData(arr.map(placeDataRaw => toPlaceData(placeDataRaw, user)), user)),
        shareReplay(1));
  }
}
