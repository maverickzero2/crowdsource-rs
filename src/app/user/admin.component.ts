import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { Relawan, USER_ROLE, computeUserChangesCount, ago } from '../rs/rs.component';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { debounceTime, map, shareReplay, switchMap } from 'rxjs/operators';
import { UtilService } from '../util.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent {
  USER_ROLE = USER_ROLE;
  users$: Observable<Relawan[]>;
  name = '';
  nameFilterTrigger$ = new BehaviorSubject('');
  filterUserRole: USER_ROLE;
  sortBy = 'numRc';
  ago = ago;

  constructor(
    public userService: UserService,
    public utilService: UtilService,
  ) {
    const name$ = this.nameFilterTrigger$.pipe(debounceTime(500), map(name => name.trim().toLowerCase()));
    this.users$ =
      this.userService.user$.pipe(
        switchMap(_ => combineLatest(name$, this.utilService.allUserData$, this.utilService.placeDataById$)),
        map(([name, users, placeDataById]) => {
          if (name.length > 0) {
            users = users.filter(u => u.nameLowerCase.indexOf(name) !== -1);
          }
          if (this.filterUserRole in USER_ROLE) {
            users = users.filter(u => u.role === this.filterUserRole);
          }
          users = users
            .map(user => computeUserChangesCount(user, placeDataById))
            .map(user => {
              user.numPending = 0;
              for (const [placeId, changes] of Object.entries(user.changes || {})) {
                const latest = placeDataById[placeId]?.latest;
                if (!latest) continue;
                for (const [key, history] of Object.entries(changes)) {
                  if (latest[key] && latest[key]?.ts >= history.ts) continue;
                  if (!!latest[key]?.value !== !!history.value) {
                    user.numPending++;
                  }
                }
              }
              return user;
            });

          if (this.sortBy === 'regDate')
            return users.sort((a, b) => b.registerDate.seconds - a.registerDate.seconds);

          if (this.sortBy === 'numPending')
            return users.sort((a, b) => {
              const diff = b.numPending - a.numPending;
              return diff ? diff : (b.registerDate.seconds - a.registerDate.seconds);
            });

          return users.sort((a, b) => {
            const nrc = b.numRsChanged - a.numRsChanged;
            if (nrc) return nrc;
            const nfc = b.numFieldsChanged - a.numFieldsChanged;
            if (nfc) return nfc;
            return a.nameLowerCase <= b.nameLowerCase ? -1 : 1;
          });
        }),
        shareReplay(1)
      );
  }
}
