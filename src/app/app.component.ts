import { Component, ChangeDetectorRef, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import { UserService } from './user.service';
import { MediaMatcher } from '@angular/cdk/layout';
import { MatSidenavContainer } from '@angular/material/sidenav';
import { MatToolbar } from '@angular/material/toolbar';
import { CdkScrollable } from '@angular/cdk/scrolling';
import { Subscription } from 'rxjs';
import { UtilService } from './util.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { filter, map } from "rxjs/operators";
import { pages } from './page/page.component';

export const appTitle = 'Kawal Rumah Sakit (by KawalCOVID19)';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit, OnDestroy {
  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  @ViewChild(MatSidenavContainer, {static: true}) sidenavContainer: MatSidenavContainer;
  @ViewChild(CdkScrollable, {static: true}) scrollable: CdkScrollable;
  @ViewChild('bottomToolbar',  {static: false}) bottomToolbar: MatToolbar;
  scrollSub: Subscription;
  scrollPosition:number = 0;

  constructor(
    public userService: UserService,
    public utilService: UtilService,
    public location: Location,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher)
  {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.router.events.pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => {
          let child = this.activatedRoute.firstChild;
          while(child){
            if(child.firstChild){
              child = child.firstChild;
            }else if(child.snapshot.data && child.snapshot.data['title']){
              if(child.snapshot.data['title'] === 'page'){
                return pages.filter(p=> p.slug === child.snapshot.params.slug)[0].title;
              }
              return child.snapshot.data['title'];
            }else{
              return null;
            }
          }
          return null;
        })
    ).subscribe( (data: any) => {
        if (data) {
            // set browser title
            this.titleService.setTitle(data + ' - ' + appTitle);
            // navtop title for mobile
            this.utilService.setTitle(data)
        }
    });
  }
  ngAfterViewInit(){
    if(this.mobileQuery.matches){
      this.scrollSub = this.scrollable.elementScrolled().subscribe((evt) => {
        const scrollTop = this.sidenavContainer.scrollable.getElementRef().nativeElement.scrollTop;
        const viewportHeight = this.sidenavContainer.scrollable.getElementRef().nativeElement.scrollHeight - window.innerHeight;
        if(scrollTop > this.scrollPosition){ // Scroll Down
          // check on the top or bottom, if yes: show bottom toolbar
          if(scrollTop == 0 || scrollTop == viewportHeight){
            this.bottomToolbar._elementRef.nativeElement.classList.add('show-toolbar');
            this.bottomToolbar._elementRef.nativeElement.classList.remove('hide-toolbar');
          }else{
            this.bottomToolbar._elementRef.nativeElement.classList.add('hide-toolbar');
            this.bottomToolbar._elementRef.nativeElement.classList.remove('show-toolbar');
          }
        }else{ // Scroll Up
          this.bottomToolbar._elementRef.nativeElement.classList.add('show-toolbar');
          this.bottomToolbar._elementRef.nativeElement.classList.remove('hide-toolbar');
        }
        this.scrollPosition = scrollTop;
      })
    }
  }
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
    if(this.scrollSub) this.scrollSub.unsubscribe();
  }
}
