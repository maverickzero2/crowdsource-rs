export enum NUMBER_DATA_KEY {
  // Suspect
  odp = 'odp',
  pdpp = 'pdpp',
  pdpr = 'pdpr',
  pdps = 'pdps',
  covp = 'covp',
  covr = 'covr',
  covs = 'covs',

  // APDs
  maskn95 = 'maskn95',
  maskSurgical = 'maskSurgical',
  gloves = 'gloves',
  glovesSteril = 'glovesSteril',
  glovesLong = 'glovesLong',
  cap = 'cap',
  goggle = 'goggle',
  faceShield = 'faceShield',
  surgicalGown = 'surgicalGown',
  coverall = 'coverall',
  hazmats = 'hazmats',
  apron = 'apron',
  boots = 'boots',
  shoeCover = 'shoeCover',
  handSanitizer = 'handSanitizer',
  handSoap = 'handSoap',
  termometerInfrared = 'termometerInfrared',
  ventilator = 'ventilator',

  // Drugs
  vitaminC = 'vitaminC',
  vitaminE = 'vitaminE',

  // Rooms
  icu = 'icu',
  isolasi = 'isolasi',
  kondisi = 'kondisi',

  // Available Tools | Stock for Isolation Room
  availableVentilator = 'availableVentilator',
  availableMechanicalVentilator = 'availableMechanicalVentilator',
  availableEcmo = 'availableEcmo',

  // Memakai ruang/alat
  usingIsolation = 'usingIsolation',
  usingIcu = 'usingIcu',
  usingVentilator = 'usingVentilator',

  // Informasi RS
  isRujukan = 'isRujukan',
  jumlahNakes = 'jumlahNakes',

}

export enum STRING_DATA_KEY {
  contact = 'contact',
  contactName = 'contactName',
  contactEmail = 'contactEmail',
  jenisLayanan = 'jenisLayanan',
  statusKepemilikan = 'statusKepemilikan'
}

export enum ITEM_STATUS {
  unset = 'unset',  // Status is unset (default).
  out = 'out',      // Out of stock, urgent status.
  short = 'short',  // Short supply, need the items in 14 days.
  ok = 'ok',        // Items no longer needed.
}

export type HistoryValue<T> = {
  status: ITEM_STATUS,  // The urgency status of the item.
  value: T,             // The value of the edit.
  uid: string,          // The user who made the edit.
  ts: number,           // The timestamp of the edit (in milliseconds).
};

export type HistoryStrings = { [key in STRING_DATA_KEY]: HistoryValue<string>[] };
export type HistoryNumbers = { [key in NUMBER_DATA_KEY]: HistoryValue<number>[] };
export interface History extends HistoryStrings, HistoryNumbers { }

export type HistoryString = { [key in STRING_DATA_KEY]: HistoryValue<string> };
export type HistoryNumber = { [key in NUMBER_DATA_KEY]: HistoryValue<number> };
export interface LatestHistory extends HistoryString, HistoryNumber { }

export function isNumberDataKey(key: string): key is keyof typeof NUMBER_DATA_KEY {
  return key in NUMBER_DATA_KEY;
}

export function isItemStatusKey(key: string): key is keyof typeof ITEM_STATUS {
  return key in ITEM_STATUS;
}

export function isStringDataKey(key: string): key is keyof typeof STRING_DATA_KEY {
  return key in STRING_DATA_KEY;
}

export interface KpData {
  id: number;
  name: string;
  depth: number;
  parentIds: number[];
  parentNames: string[];
  children: { [cid: number]: string };
  setBy: string; // The uid of the setter of this KpData.
}

export enum STATUS_KEPEMILIKAN_RS {
  pemerintah = 'Pemerintah',
  swasta = 'Swasta',
  tniPolri = 'TNI/POLRI'
}

export enum JENIS_LAYANAN_RS {
  rs = 'Rumah Sakit',
  puskesmas = 'Puskesmas',
  klinik = 'Klinik'
}

export interface PlaceData {
  // Google Map's Place Details.
  place: google.maps.places.PlaceResult;

  // Changes to this place done by Relawan.
  history: History;

  // Contains the latest value in the history (computed locally).
  latest: LatestHistory;

  // Data lokasi KawalPemilu.org
  kpData: KpData;
}

export interface KvDetail {
  key: string;
  shortLabel: string;

  // Deprecated. Please use "latest" field.
  value: number;

  unitLabel?: string;

  // The latest value of the key.
  latest: HistoryValue<string | number>;
}

export interface RsData {
  placeData: PlaceData;
  suspects: KvDetail[];
  logistics: KvDetail[];
  beds: KvDetail[];
  availabilities: KvDetail[];

  // Jumlah semua kebutuhan di RS ini (termasuk yang mendesak)
  totalKebutuhan: number;

  // Jumlah kebutuhan yang mendesak di RS ini.
  totalMendesak: number;

  // The distance in KM to the user's location (if enabled).
  distanceToUser: number;

  // The largest timestamp of any cell updated in this RS.
  lastUpdateTs: number;
}

export function isNumberKey(k: string): k is NUMBER_DATA_KEY {
  return k in NUMBER_DATA_KEY;
}

export function isHistoryKey(k: string): k is STRING_DATA_KEY | NUMBER_DATA_KEY {
  return k in STRING_DATA_KEY || k in NUMBER_DATA_KEY;
}

interface LatLng {
  lat: number;
  lng: number;
}

// Get Location Distance between two coordinates
// Source: https://www.movable-type.co.uk/scripts/latlong.html
// @params pointA, pointB, unit: 'K' for kilometers 'M' for meters
// @return distance by requested unit
function calculateDistance(pointA: LatLng, pointB: LatLng, unit = 'K') {
  const radlat1 = Math.PI * pointA.lat / 180;
  const radlat2 = Math.PI * pointB.lat / 180;
  const radtheta = (pointB.lng - pointA.lng) * Math.PI / 180;
  let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  dist = Math.acos(dist);
  dist = dist * 180 / Math.PI;
  dist = dist * 60 * 1.1515;
  if (unit === "K") { dist = dist * 1.609344 }
  if (unit === "M") { dist = dist * 0.8684 }
  return dist;
}

function getDistance(userLocation: { lat: number, lng: number } | undefined | null, placeData: PlaceData) {
  return !userLocation ? 0 : calculateDistance(
    userLocation,
    // @ts-ignore
    { lat: placeData.place.geometry.location.lat, lng: placeData.place.geometry.location.lng }
  );
}

// Compute the latest value from PlaceData history.
// The userChanges may override PlaceData.
export function toPlaceData(obj: any, relawan: Relawan | null = null): PlaceData {
  const placeData = { ...obj, latest: {} } as PlaceData;
  for (const [k, v] of Object.entries<any[]>(obj.history ?? {})) {
    if (isHistoryKey(k) && v.length > 0) {
      placeData.latest[k] = v[v.length - 1];
    }
  }
  for (const [placeId, history] of Object.entries(relawan?.changes || {})) {
    if (placeData.place.place_id !== placeId) continue;
    for (const [key, { status, value, ts }] of Object.entries(history)) {
      if (!isHistoryKey(key)) continue;
      if (!placeData.latest[key] || placeData.latest[key].ts < ts) {
        // @ts-ignore
        placeData.latest[key] = { status, value, ts };
      }
    }
  }
  return placeData;
}

export function toRsData(arr: PlaceData[], relawan: Relawan | null = null): RsData[] {
  const groupKey: { [key: string]: string } = {}, shortLabel: any = {}, unitLabel: any = {};
  for (const a of Attributes.suspect) {
    groupKey[a.key] = 'suspects';
    shortLabel[a.key] = a.name || a.shortLabel;
  }
  for (const a of Attributes.logistic) {
    groupKey[a.key] = 'logistics';
    shortLabel[a.key] = a.name || a.shortLabel;
    unitLabel[a.key] = a.unitLabel;
  }
  for (const a of Attributes.bed) {
    groupKey[a.key] = 'beds';
    shortLabel[a.key] = a.name || a.shortLabel;
  }
  for (const a of Attributes.availibility) {
    groupKey[a.key] = 'availabilities';
    shortLabel[a.key] = a.name || a.shortLabel;
    unitLabel[a.key] = a.unitLabel;
  }

  const allData: RsData[] = [];
  for (const placeData of arr) {
    const rsData: RsData = {
      placeData,
      suspects: [],
      logistics: [],
      beds: [],
      availabilities: [],
      totalKebutuhan: 0,
      totalMendesak: 0,
      distanceToUser: 0,
      lastUpdateTs: 0,
    };
    for (const key of Object.keys(placeData.latest)) {
      if (!groupKey[key]) continue;
      if (!isNumberDataKey(key)) {
        console.warn('Grouping non number', key);
        continue;
      }
      const latest = placeData.latest[key];
      const value = +latest.value;
      if (isNaN(value) || value <= 0) continue; // Ignores empty or invalid values.
      // @ts-ignore
      const kvArr: KvDetail[] = rsData[groupKey[key]] as KvDetail[];
      kvArr.push({
        key,
        shortLabel: shortLabel[key],
        value: value,
        unitLabel: unitLabel[key] || '',
        latest,
      });
      rsData.totalKebutuhan += value;
      if (latest.status === ITEM_STATUS.out) {
        rsData.totalMendesak += value;
      }
      rsData.lastUpdateTs = Math.max(rsData.lastUpdateTs, latest.ts);
    }
    rsData.distanceToUser = getDistance(relawan?.location, placeData);
    if (rsData.totalKebutuhan > 0) allData.push(rsData);
  }
  console.log('hospitals', arr.length, 'punya kebutuhan', allData.length);
  return allData.sort((a, b) => {
    const diff = b.totalKebutuhan - a.totalKebutuhan;
    if (diff) return diff;
    const aId = a?.placeData?.place?.place_id ?? '';
    const bId = b?.placeData?.place?.place_id ?? '';
    return aId <= bId ? -1 : 1;
  });
}

export type RsHistory = {
  [key: string]: {
    status: ITEM_STATUS,
    value: string | number,
    ts: number
  }
}

export type UserChanges = { [placeId: string]: RsHistory };

export enum USER_ROLE {
  BANNED = -1,    // Can view tabulation (this is equivalent to not-logged in users).
  PUBLIC = 0,     // Can search and edit RS data in their workspace (not visible globally until approved).
  RELAWAN = 1,    // Can search and edit RS data globally (all their workspace edit becomes global).
  MODERATOR = 2,  // Can promote PUBLIC -> RELAWAN.
  ADMIN = 3,      // Can promote PUBLIC/RELAWAN -> MODERATOR.
}

export function computeUserChangesCount(relawan: Relawan, placeById?: { [placeId: string]: PlaceData }) {
  relawan.numRsChanged = 0;
  relawan.numFieldsChanged = 0;
  for (const changes of Object.values(relawan.changes || {})) {
    let hasChange = 0;
    for (const _ of Object.values(changes)) {
      // TODO: do not count if the data is overriden?
      relawan.numFieldsChanged++;
      hasChange = 1;
    }
    relawan.numRsChanged += hasChange;
  }
  return relawan;
}

export type SumByStatus = { [key in ITEM_STATUS]: number };

export interface AggData {
  // Aggregated sum value by item status (if any).
  sumByStatus: SumByStatus;
  maxTs: number;    // The maximum timestamp.
}

export type AggFaskesByNumKey = {
  [key in NUMBER_DATA_KEY]: AggData
};

export interface AggHieData extends AggFaskesByNumKey {
  // Number of faskes in this subtree.
  count: number;
}

export interface HierarchyData {
  // Data lokasi KawalPemilu.org
  kpData: KpData;

  // The aggregated data at this node.
  agg: AggHieData;

  // The aggregated data of each children.
  childHieData: { [childId: number]: HierarchyData };

  // For the lowest level (kelurahan), the entire RS data is stored.
  rsData: { [placeId: string]: PlaceData };
}

export enum REGISTRATION_KEY {
  profesi = 'profesi',
  profesiLainnya = 'profesiLainnya',
  instansi = 'instansi',
  regEmail = 'regEmail',
  noHp = 'noHp',
  fotoIdCard = 'fotoIdCard',
  fotoIdCardMeta = 'fotoIdCardMeta',
  deskripsi = 'deskripsi',
}

export type RegistrationInfo = { [key in REGISTRATION_KEY]: string };

export interface ImageMetadata {
  l: number; // Last Modified Timestamp.
  a: number; // Created Timestamp.
  s: number; // Size in Bytes.
  z: number; // Size in Bytes after compressed.
  w: number; // Original Width.
  h: number; // Original Height.
  m: [string, string]; // [Make, Model].
  o: number; // Orientation.
  y: number; // Latitude.
  x: number; // Longitude.
}

export function ago(ts: number) {
  let elapsed = (Date.now() - ts) / 1000;
  if (elapsed < 60) return Math.floor(elapsed) + 's lalu';
  elapsed /= 60;
  if (elapsed < 60) return Math.floor(elapsed) + 'm lalu';
  elapsed /= 60;
  if (elapsed < 24) return Math.floor(elapsed) + 'h lalu';
  return `${Math.floor(elapsed / 24)}d ${Math.floor(elapsed % 24)}h lalu`;
}

// Returns the error message. Otherwise empty string.
export function validateBulkEdit(values: any) {
  for (const [key, { value, status }] of Object.entries<any>(values)) {
    if (key === 'kpId') continue;
    if (!(status in ITEM_STATUS)) return `invalid status: ${status}`;
    if (isNumberDataKey(key)) {
      const numValue = +value;
      if (isNaN(numValue) || numValue < 0) return `invalid number value: ${value} for ${key}`;
    } else if (isStringDataKey(key)) {
      if (typeof value !== 'string') return `invalid string value for ${key}`;
    } else {
      return `invalid key: ${key}`;
    }
  }
  return '';
}

export function extractImageMetadata(m: any): ImageMetadata | null {
  let validM: ImageMetadata | null = null;
  if (m) {
    validM = {} as ImageMetadata;
    ['k', 't', 'l', 'a', 's', 'z', 'w', 'h', 'o', 'y', 'x'].forEach(
      attr => {
        if (typeof m[attr] === 'number') {
          // @ts-ignore
          validM[attr] = m[attr];
        }
      }
    );
    if (typeof m.m === 'object') {
      validM.m = ['', ''];
      if (typeof m.m[0] === 'string') {
        validM.m[0] = m.m[0].substring(0, 50);
      }
      if (typeof m.m[1] === 'string') {
        validM.m[1] = m.m[1].substring(0, 50);
      }
    }
  }
  return !validM || Object.keys(validM).length === 0 ? null : validM;
}

export interface Relawan {
  displayName: string | null;
  email: string | null;
  phoneNumber: string | null;
  photoURL: string | null;
  providerId: string;
  uid: string;

  userid: string;
  nameLowerCase: string;
  role: USER_ROLE;
  registerDate: any; // { seconds: number, nanoseconds: number };

  numRsChanged: number;       // Number of RS that were changed by this user.
  numFieldsChanged: number;   // Number of cells edited by this user.
  numPending: number;         // Number of edit cells that are in pending review.

  registration: RegistrationInfo;

  // Relawan's contributions.
  changes: UserChanges | null;

  // Is the user chose to enable location?
  enable_location: boolean;

  // User location: null means denied.
  location: { lat: number, lng: number } | null;
}

export interface AttributeDetail {
  key: string;
  label: string;
  name?: string;
  longLabel?: string;
  shortLabel: string;
  formLabel: string;
  isPublic: boolean;
  show?: boolean;
  unitLabel?: string;
}

export const Attributes: { [key: string]: AttributeDetail[] } = {
  suspect: [
    // Suspect
    {
      key: 'odp',
      label: 'ODP',
      longLabel: 'Orang Dalam Pemantauan',
      shortLabel: 'ODP',
      formLabel: 'Jumlah Orang Dalam Pemantauan',
      isPublic: false
    },
    {
      key: 'pdpp',
      label: 'PDP Meninggal',
      longLabel: 'Pasien Dalam Pengawasan yang Meninggal',
      formLabel: 'Jumlah Pasien Dalam Pengawasan yang Meninggal',
      shortLabel: 'PDP+',
      isPublic: false
    },
    {
      key: 'pdpr',
      label: 'PDP Dirawat',
      longLabel: 'Pasien Dalam Pengawasan yang Dirawat',
      formLabel: 'Jumlah Pasien Dalam Pengawasan yang Dirawat',
      shortLabel: 'PDP',
      isPublic: false
    },
    {
      key: 'pdps',
      label: 'PDP Sembuh',
      longLabel: 'Pasien Dalam Pengawasan yang Sembuh',
      formLabel: 'Jumlah Pasien Dalam Pengawasan yang Sembuh',
      shortLabel: 'PDP*',
      isPublic: false
    },
    {
      key: 'covp',
      label: 'Positif Covid-19 Meninggal',
      longLabel: 'Pasien Positif Covid-19 yang Meninggal',
      formLabel: 'Jumlah Pasien Positif Covid-19 yang Meninggal',
      shortLabel: 'Cov+',
      isPublic: false
    },
    {
      key: 'covr',
      label: 'Positif Covid-19 Dirawat',
      longLabel: 'Pasien Positif Covid-19 yang Dirawat',
      formLabel: 'Jumlah Pasien Positif Covid-19 yang Dirawat',
      shortLabel: 'Cov',
      isPublic: false
    },
    {
      key: 'covs',
      label: 'Positif Covid-19 Sembuh',
      longLabel: 'Pasien Positif Covid-19 yang Sembuh',
      formLabel: 'Jumlah Pasien Positif Covid-19 yang Sembuh',
      shortLabel: 'Cov*',
      isPublic: false
    },
  ],
  logistic: [
    // APDs and logistics
    {
      key: 'maskSurgical',
      name: 'Surgical Mask',
      shortLabel: 'R.SM',
      label: 'Kebutuhan Surgical Mask',
      formLabel: 'Jumlah Surgical Mask yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'maskn95',
      name: 'N95 Mask',
      shortLabel: 'R.N95',
      label: 'Kebutuhan N95 Mask',
      formLabel: 'Jumlah N95 Mask yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'gloves',
      name: 'Sarung Tangan',
      shortLabel: 'R.ST',
      label: 'Kebutuhan Sarung Tangan',
      formLabel: 'Jumlah Sarung tangan non-steril yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'glovesSteril',
      name: 'Sarung Tangan Steril',
      shortLabel: 'R.STT',
      label: 'Kebutuhan Sarung Tangan Steril',
      formLabel: 'Jumlah Sarung tangan steril yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'glovesLong',
      name: 'Sarung Tangan Panjang',
      shortLabel: 'R.STP',
      label: 'Kebutuhan Sarung Tangan Panjang',
      formLabel: 'Jumlah Sarung tangan panjang (Long Handscoon) yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'cap',
      name: 'Pelindung Kepala (cap)',
      shortLabel: 'R.Cap',
      label: 'Kebutuhan Pelindung Kepala (cap)',
      formLabel: 'Jumlah Pelindung Kepala (cap) yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'goggle',
      name: 'Goggle',
      shortLabel: 'R.Gog',
      label: 'Kebutuhan Goggle',
      formLabel: 'Jumlah Goggle yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'faceShield',
      name: 'Face Shield',
      shortLabel: 'R.FC',
      label: 'Kebutuhan Face Shield',
      formLabel: 'Jumlah Face Shield yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'surgicalGown',
      name: 'Surgical Gown',
      shortLabel: 'R.SG',
      label: 'Kebutuhan Surgical Gown',
      formLabel: 'Jumlah Surgical Gown yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'coverall',
      name: 'Coverall',
      shortLabel: 'R.Coverall',
      label: 'Kebutuhan Coverall',
      formLabel: 'Jumlah Coverall yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'hazmats',
      name: 'Baju Hazmat',
      shortLabel: 'R.Haz',
      label: 'Kebutuhan Baju Hazmat',
      formLabel: 'Jumlah Baju Hazmat yang dibutuhkan',
      isPublic: true,
      show: true,
      unitLabel: 'pcs',
    },
    {
      key: 'apron',
      name: 'Apron',
      shortLabel: 'R.Apr',
      label: 'Kebutuhan Apron',
      formLabel: 'Jumlah Apron yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'boots',
      name: 'Sepatu Boots',
      shortLabel: 'R.Boots',
      label: 'Kebutuhan Sepatu Boots',
      formLabel: 'Jumlah Sepatu Boots yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pasang',
    },
    {
      key: 'shoeCover',
      name: 'Shoe Cover',
      shortLabel: 'R.SC',
      label: 'Kebutuhan Shoe Cover',
      formLabel: 'Jumlah Shoe Cover yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'handSanitizer',
      name: 'Hand Sanitizer',
      shortLabel: 'R.HSan',
      label: 'Kebutuhan Hand Sanitizer',
      formLabel: 'Jumlah Hand Sanitizer yang dibutuhkan',
      isPublic: true,
      unitLabel: 'botol 250cc',
    },
    {
      key: 'handSoap',
      name: 'Sabun Cuci Tangan',
      shortLabel: 'R.HSoap',
      label: 'Kebutuhan Sabun Cuci Tangan',
      formLabel: 'Jumlah sabun cuci tangan yang dibutuhkan',
      isPublic: true,
      unitLabel: 'botol',
    },
    {
      key: 'termometerInfrared',
      name: 'Termometer Infrared',
      shortLabel: 'R.Ter',
      label: 'Kebutuhan Termometer Infrared',
      formLabel: 'Jumlah Termometer Infrared yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'ventilator',
      name: 'Ventilator',
      shortLabel: 'R.Vent',
      label: 'Kebutuhan Ventilator',
      formLabel: 'Jumlah Ventilator yang dibutuhkan',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'vitaminC',
      name: 'Vitamin C',
      shortLabel: 'Vit C',
      label: 'Kebutuhan Vitamin C',
      formLabel: 'Jumlah Vitamin C yang dibutuhkan',
      isPublic: true,
      unitLabel: 'box',
    },
    {
      key: 'vitaminE',
      name: 'Vitamin E',
      shortLabel: 'Vit E',
      label: 'Kebutuhan Vitamin E',
      formLabel: 'Jumlah Vitamin E yang dibutuhkan',
      isPublic: true,
      unitLabel: 'box',
    },
  ],
  bed: [
    // Rooms
    {
      key: 'icu',
      shortLabel: 'ICUs',
      label: 'Kapasitas Tempat Tidur Ruang ICU',
      formLabel: 'Kapasitas ruang ICU di rumah sakit ini',
      isPublic: true
    },
    {
      key: 'isolasi',
      shortLabel: 'Isolations',
      label: 'Kapasitas Tempat Tidur Ruang Isolasi',
      formLabel: 'Kapasitas ruang isolasi di rumah sakit ini',
      isPublic: true
    },
    {
      key: 'kondisi',
      shortLabel: 'Overloaded',
      label: 'Ketersediaan Tempat Tidur',
      formLabel: 'Daya tampung tempat tidur/ruangan',
      isPublic: true
    },
  ],
  availibility: [
    // Availibity tools for support intensive care / isolation
    {
      key: 'availableVentilator',
      shortLabel: 'Av.Vent',
      longLabel: 'Ketersediaan Ventilator',
      label: 'Ventilator',
      formLabel: 'Ketersediaan Non Invasive Ventilator di rumah sakit ini',
      isPublic: true,
      unitLabel: 'pcs',
    },
    {
      key: 'availableMechanicalVentilator',
      shortLabel: 'Av.MVent',
      longLabel: 'Ketersediaan Mechanical Ventilator',
      label: 'Mechanical Ventilator',
      formLabel: 'Ketersediaan Mechanical Ventilator di rumah sakit ini',
      isPublic: true,
      unitLabel: 'unit',
    },
    {
      key: 'availableEcmo',
      shortLabel: 'Av.ECMO',
      longLabel: 'Ketersediaan Extracorporeal Membrane Oxygenation (ECMO)',
      label: 'ECMO',
      formLabel: 'Ketersediaan Extracorporeal Membrane Oxygenation (ECMO) di rumah sakit ini',
      isPublic: true,
      unitLabel: 'unit',
    }
  ],
  use: [
    {
      key: 'usingIsolation',
      shortLabel: 'Ruang Isolasi',
      longLabel: 'Ruang Isolasi Terpakai',
      label: 'Ruang Isolasi',
      formLabel: 'Jumlah pasien yang menggunakan ruang isolasi saat ini',
      isPublic: true,
    },
    {
      key: 'usingIcu',
      shortLabel: 'Ruang ICU',
      longLabel: 'Ruang ICU Terpakai',
      label: 'Ruang ICU',
      formLabel: 'Jumlah pasien yang menggunakan ruang ICU saat ini',
      isPublic: true,
    },
    {
      key: 'usingVentilator',
      shortLabel: 'Ventilator Terpakai',
      longLabel: 'Ventilator Terpakai',
      label: 'Ventilator Terpakai',
      formLabel: 'Jumlah ventilator yang terpakai di rumah sakit saat ini',
      isPublic: true,
    },
  ],
  info: [
    { key: 'isRujukan', shortLabel: 'Rumah Sakit Rujukan', label: '', formLabel: '', isPublic: true, },
    { key: 'jenisLayanan', shortLabel: 'Jenis Layanan', label: '', formLabel: '', isPublic: true, },
    { key: 'statusKepemilikan', shortLabel: 'Status Kepemilikan', label: '', formLabel: '', isPublic: true, },
    { key: 'jumlahNakes', shortLabel: 'Jumlah Tenaga Kesehatan', label: '', formLabel: '', isPublic: true, },
    { key: 'contactName', shortLabel: 'Nama Penerima Donasi', label: '', formLabel: '', isPublic: true, },
    { key: 'contactEmail', shortLabel: 'Email Penerima Donasi', label: '', formLabel: '', isPublic: true, },
    { key: 'contact', shortLabel: 'Nomor Kontak RS', label: '', formLabel: '', isPublic: true, },
  ]
};
