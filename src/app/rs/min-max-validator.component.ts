import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-min-max-validator',
  template: `
<div *ngIf="control.invalid && (control.dirty || control.touched)" class="alert alert-danger">
  <div *ngIf="control.errors.min">
    {{ name }} minimum adalah 0.
  </div>
  <div *ngIf="control.errors.max">
    {{ name }} maximum adalah {{ maxValue }}.
  </div>
  <div *ngIf="control.errors.pattern">
    Angka harus berupa bilangan bulat.
  </div>
</div>
  `,
})
export class MinMaxValidatorComponent implements OnInit {
  @Input() control: AbstractControl;
  @Input() name: string;
  @Input() maxValue: number;

  constructor() { }

  ngOnInit(): void {
  }
}
