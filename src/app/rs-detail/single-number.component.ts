import { Component, Input, Inject, OnInit, OnChanges, ViewChild, ElementRef, AfterContentInit, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PlaceData, ITEM_STATUS, HistoryValue, ago } from '../rs/rs.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilService } from '../util.service';
import { UserService } from '../user.service';

interface CellHistory extends HistoryValue<number> {
  ago: string;
}

export interface DialogData {
  title: string;
  placeData: PlaceData;
  key: string;
  unitLabel?: string;
  hold: boolean;      // True will not close the dialog until it's submitted.
  hasStatus: boolean; // True will display the item status.
}

@Component({
  selector: 'app-single-number-card',
  template: `
    <mat-card matRipple [matRippleCentered]="true"
      class="text-center card-clickable" style="cursor: pointer"
      (click)="openDialog()">
      <div style="margin-bottom: 0px;">
        <span class="text-subtitle">{{ subtitle }}</span>
        <mat-spinner style="margin: 0 auto" [diameter]="35" *ngIf="updating"></mat-spinner>
        <h1 class="lead {{numberClass}}" *ngIf="!updating">
          {{ placeData.latest[key]?.value || 0 | number:'1.0':'id' }}<span>{{unitLabel}}</span>
        </h1>
        <span *ngIf="placeData.latest[key]" class="ts">{{utilService.timeAgo(placeData.latest[key].ts)}}</span>
        <p *ngIf="hasStatus && placeData.latest[key]?.status === ITEM_STATUS.out"
          style="color:red; font-weight: bold">(Out Of Stock)</p>
      </div>
    </mat-card>
`,
  styles: [`
    .text-subtitle{
      font-size:1rem;
      line-height:1.4rem;
    }
    .success{
      color:#219653;
    }
    .warning-1{
      color:#F2C94C;
    }
    .warning-2{
      color:#F2994A;
    }
    .warning-3{
      color:#D8232A;
    }
    .lead {
      margin: 10px 0px;
      display:flex;
      flex-flow:column;
      font-size: 2.4rem;
    }
    .lead span{
      font-size:.8rem;
      color:#ccc;
    }
    .ts{
      font-size:.6rem;
      color:#666B73;
    }
    .card-clickable:hover{
      background-color:#E5E5E5;
    }
`]
})
export class SingleNumberCard implements OnInit, OnChanges {
  @Input() placeData: PlaceData;
  @Input() key: string;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() unitLabel: string;
  @Input() hold = false;
  @Input() hasStatus = false;
  @Input() updatedAt: number;
  @Input() numberClass: string = 'warning-1';

  ITEM_STATUS = ITEM_STATUS;

  bgcolor = '';
  updating = false;

  constructor(
    public dialog: MatDialog,
    public userService: UserService,
    public utilService: UtilService,
  ) { }

  ngOnChanges() {
    this.bgcolor = this.utilService.getColor(this.placeData.latest[this.key]?.ts);
  }

  ngOnInit() {
    this.ngOnChanges();
  }

  // title: string, unitLabel: string, placeData: PlaceData, key: string, value: string, hold = false
  openDialog(data?: DialogData): void {
    if (data === undefined) {
      data = {
        placeData: this.placeData,
        title: this.title,
        unitLabel: this.unitLabel,
        key: this.key,
        hold: this.hold,
        hasStatus: this.hasStatus,
      };
    }

    if (!this.userService.isPublic) {
      console.warn('Only relawan can edit');
      return;
    }

    const dialogRef = this.dialog.open(SingleNumberDialog, {
      maxWidth: '100vh',
      width: this.utilService.isHandset ? '80vh' : '50vh',
      position: { 'bottom': this.utilService.isHandset ? '0px' : '' },
      disableClose: true,
      autoFocus: true,
      data
    });

    dialogRef.afterClosed().subscribe(async result$ => {
      console.log('The dialog was closed', result$);
      if (!result$) return;
      this.updating = true;
      try {
        const result = await result$;
        if (result.error) {
          alert(result.error);
        }
      } catch (e) {
        alert(e.message);
        console.error(e);
      }
      this.updating = false;
    });
  }
}

@Component({
  selector: 'app-demand-card',
  template: `
    <mat-card matRipple [matRippleCentered]="true" class="card-clickable"
      style="cursor: pointer; margin-bottom:10px;"
      (click)="openDialog()">
      <mat-spinner style="margin: 0 auto" [diameter]="35" *ngIf="updating"></mat-spinner>
      <div class="demand-item" *ngIf="!updating">
        <div class="item-primary">
          <h3>{{ subtitle }}</h3>
          <h1>{{ placeData.latest[key]?.value || 0 | number:'1.0':'id' }}</h1>
        </div>
        <div class="item-secondary">
          <span class="ts">Dimutakhirkan {{utilService.timeAgo(placeData.latest[key].ts)}}</span>
          <span class="text-right">{{unitLabel}}</span>
        </div>
      </div>

    </mat-card>
`,
  styles: [`
    .demand-item .item-primary, .demand-item .item-secondary{
      display:flex;
      flex-flow:row;
      justify-content:space-between;
      align-items:baseline;
      margin:0;
    }
    .demand-item h3{
      font-size: 1rem;
      line-height:1rem;
      margin:0;
      padding:0;
    }
    .demand-item h1{
      font-size: 1.4rem;
      line-height:1.4rem;
      font-weight:400;
      margin:0;
      padding:0;
    }
    .demand-item .item-right{
      text-align:right;
    }
    .demand-item .item-right span{
      font-size:.8rem;
      color:#777;
    }
    .demand-item .item-left span{
      align-self: flex-end;
    }
    .ts{
      font-size:.6rem;
      color:#666B73;
    }
    .card-clickable:hover{
      background-color:#E5E5E5;
    }
`]
})
export class DemandCard extends SingleNumberCard implements OnInit, OnChanges {
  @Input() placeData: PlaceData;
  @Input() key: string;
  @Input() title: string;
  @Input() subtitle: string;
  @Input() unitLabel: string;
  @Input() hold = false;
  @Input() hasStatus = false;

  ITEM_STATUS = ITEM_STATUS;

  bgcolor = '';
  updating = false;

  constructor(
    public dialog: MatDialog,
    public userService: UserService,
    public utilService: UtilService,
  ) {
    super(dialog, userService, utilService)
  }
}

@Component({
  selector: 'app-single-number-dialog',
  template: `
    <div mat-dialog-content [formGroup]="form" class="single-number-dialog">
      <p>{{ data.title }}</p>

      <mat-form-field *ngIf="data.hasStatus">
        <mat-label>Status Barang Sekarang</mat-label>
        <mat-select formControlName="status" (selectionChange)="statusChanged()">
          <mat-option [value]="ITEM_STATUS.short">Akan Habis</mat-option>
          <mat-option [value]="ITEM_STATUS.out">Sudah Habis</mat-option>
          <mat-option [value]="ITEM_STATUS.ok">Terpenuhi</mat-option>
        </mat-select>
      </mat-form-field>

      <div [hidden]="data.hasStatus && itemStatus === ITEM_STATUS.ok">
        <mat-form-field>
          <mat-label *ngIf="data.hasStatus">Kebutuhan Tambahan</mat-label>
          <input matInput #input formControlName="value" type="number"
            (ngModelChange)="valueChanged()"
            (keyup.enter)="submit(form.value)" autocomplete="off"
            [disabled]="disabled" cdkFocusInitial>
            <span *ngIf="data.unitLabel" matSuffix style="color:#ccc;margin-left:10px;">{{data.unitLabel}}</span>
            <mat-hint *ngIf="data.hasStatus">
              Harap estimasikan kebutuhan tambahan untuk <strong>1 bulan</strong> ke depan
            </mat-hint>
        </mat-form-field>
        <app-min-max-validator [control]="form.get('value')" name="Nilai" [maxValue]="MAX2">
        </app-min-max-validator>
      </div>
      <ng-container *ngIf="userService.isModerator && cellHistory.length > 0">
        <div class="update-history">
          <strong>Riwayat perubahan</strong>
          <ul class="list-history">
            <li *ngFor="let h of cellHistory">
              <mat-icon>person_pin</mat-icon>
              <app-user-link [uid]="h.uid" (click)="dialogRef.close()"></app-user-link>
              <div class="changes">
                {{getItemStatus(h.status)}} <span class="value-number">{{h.value}}</span>
              </div>
              <span class="ago">{{h.ago}}</span>
            </li>
          </ul>
          <!--<table>
            <tr><th width="60">Kapan</th><th width="70">Value</th><th>Oleh</th></tr>
            <tr *ngFor="let h of cellHistory">
              <td align="right">{{ h.ago }}</td>
              <td align="center">{{ h.value }} <b>{{ h.status }}</b></td>
              <td><app-user-link [uid]="h.uid" (click)="dialogRef.close()"></app-user-link></td>
            </tr>
          </table>-->
        </div>
      </ng-container>
    </div>
    <div mat-dialog-actions style="justify-content:flex-end;margin-top:0px;">
    <mat-spinner [diameter]="25" *ngIf="disabled"></mat-spinner>
      <button [disabled]="disabled" mat-button (click)="dialogRef.close()">Batal</button>
      <button [disabled]="disabled" mat-raised-button color="primary" type="submit"
        (click)="submit(form.value)">Simpan</button>
    </div>
  `,
  styles: [`
    .single-number-dialog{
      display:flex;
      flex-flow:column;
    }
    .update-history{
      background-color:#f8f8f8;
      margin:24px -24px 0px -24px;
      padding:10px;
      border-top:1px solid #CDCDCD;
      border-bottom:1px solid #CDCDCD;
    }
    .update-history .mat-icon{
      font-size:16px;
      line-height:18px;
    }
    .list-history{list-style:none;margin:10px 0 0 0;padding:0;}
    .list-history li{
      width:100%;
      display:flex;
      flex-flow:row;
      margin-bottom:4px;
      font-size:.7rem;
    }
    .list-history li .changes{
      flex-grow:1;
      padding-left:10px;
    }
    .list-history li .changes span{margin:0px 5px;}
    .list-history li .changes span.value-number{font-weight:bold;}
    .list-history li .ago{
      text-align:right;
      color:#CDCDCD;
    }
  `]
})
export class SingleNumberDialog implements AfterContentInit {
  @ViewChild('input', { static: false }) inputRef: ElementRef;

  ITEM_STATUS = ITEM_STATUS;
  disabled = false;
  form: FormGroup;
  MAX2 = 999999;
  cellHistory: CellHistory[] = [];

  constructor(
    public userService: UserService,
    public dialogRef: MatDialogRef<SingleNumberDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
  ) {
    const latest: HistoryValue<string | number> = data.placeData.latest[data.key];
    this.form = this.fb.group({
      status: [latest?.status ?? (data.hasStatus ? ITEM_STATUS.short : ITEM_STATUS.unset)],
      value: [latest?.value ?? '', [
        Validators.min(0), Validators.max(this.MAX2), Validators.pattern(/^-?[0-9]{0,6}$/)]]
    });

    const hs = data.placeData.history;
    for (const h of (hs && hs[data.key])?.reverse() || []) {
      this.cellHistory.push({ ago: ago(h.ts), ...h });
      if (this.cellHistory.length >= 5) break;
    }
  }

  get itemStatus() {
    return this.form.get('status').value;
  }

  ngAfterContentInit() {
    this.cd.detectChanges();
    this.inputRef.nativeElement.select();
  }

  statusChanged() {
    if (this.itemStatus === ITEM_STATUS.ok) {
      this.form.get('value').setValue(0);
    }
  }

  valueChanged() {
    const value = this.form.get('value').value;
    if (this.data.hasStatus) {
      if (value === 0) {
        this.form.get('status').setValue(ITEM_STATUS.ok);
      }
    } else {
      this.form.get('status').setValue(ITEM_STATUS.unset);
    }
    if (!Number.isInteger(value)) {
      this.form.get('value').setValue(parseInt(value));
    }
  }

  getItemStatus(status) {
    return status === 'ok'
      ? 'Terpenuhi'
      : status === 'short'
        ? 'Segera Habis'
        : status === 'out'
          ? 'Sudah Habis'
          : 'Unset';
  }

  async submit(value) {
    const placeId = this.data.placeData.place.place_id;
    if (!this.data.hold) {
      this.dialogRef.close(
        this.userService.post(`edit/${placeId}/${this.data.key}`, value));
      return;
    }
    this.disabled = true;
    try {
      const res: any = await this.userService.post(
        `edit/${placeId}/${this.data.key}`, value);
      if (res.error) {
        alert(res.error);
      } else {
        this.dialogRef.close();
      }
    } catch (e) {
      alert(e.message);
      console.error(e);
    }
    this.disabled = false;
  }
}
