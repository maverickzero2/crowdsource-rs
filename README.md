# Overview
Tidak adanya data jelas mengenai persebaran kasus COVID-19 membuat masyarakat resah atau justru malah tidak menyadari jika ada bahaya coronavirus di dekatnya. Di sisi lain, pihak-pihak yang bertanggung jawab untuk mengambil keputusan strategis dan ingin memberikan bantuan tidak bisa menemukan target bantuan yang tepat. Kawal Rumah Sakit hadir untuk memberikan informasi yang akurat untuk menjawab kurangnya data/informasi, meliputi:
* orang terjangkit (ODP, PDP, Positif COVID-19);
* kebutuhan alat tambahan (APD dan logistik lainnya);
* informasi jumlah tempat tidur di Rumah Sakit atau Fasilitas Layanan Kesehatan (Fasyankes);
* informasi ketersediaan alat pendukung untuk penanganan COVID-19; hingga
* informasi narahubung setiap Fasyankes untuk tindak lanjut penyaluran bantuan berdasarkan informasi kebutuhan yang dipublikasikan.

Inisiatif ini bisa diakses melalui laman https://kawalrumahsakit.id yang merupakan bagian dari urun daya relawan KawalCOVID19.id

# Project Documentation

* Kami menggunakan Quip dalam proses pendokumentasian, bisa diakses di [Project Workbook](https://kawalcovid19.quip.com/UycNAuGZ9lFz/Project-Workbook)
* Design on Figma [Kawal-RS](https://www.figma.com/file/x60WefbCEFZd6XS5JOtF6v/Kawal-RS?node-id=0%3A1)
* Discussion Channel on Slack [#team-kawalrumahsakit-crowdsource-dari-rs](https://kawalcovid19.slack.com/archives/CV49G29AM)

# Stack
* [Angular](https://angular.io)
* [Firebase AngularFirestore (Auth, Store)](https://firebase.google.com)
* [UI: Angular Material](https://material.angular.io)

# Data Schema

* `users` including user profile (Facebook Login). Roles: admin, moderator, relawan, & public
* `rs` RS (suspect, logistics, beds, availibility) see details on [RS schema](https://gitlab.com/kawalcovid19/crowdsource-rs/crowdsource-rs/-/blob/master/src/app/rs/rs.component.ts)
* `hierarchy` Hierarchy (Aggregated `rs` data by wilayah administratif di Indonesia)
* `rev` each data changes logged in this collection, comprised `key` of `rs` attributes, `value`, `previous value`, `user` which revise the data, and `timestamp` log

# How to Contribute?
Anda dapat berkontribusi dalam project ini, langkahnya ada di [CONTRIBUTING.md](https://gitlab.com/kawalcovid19/handbook/-/blob/master/CONTRIBUTING.md)

Pada repo ini, akses pada seluruh data memang dibatasi (Firestore auth with facebook logged in). Untuk itu, anda perlu tercatat sebagai relawan terlebih dulu. Langkahnya:
* anda login ke https://kawalrumahsakit.id menggunakan akun Facebook anda;
* hubungi salah satu moderator (contoh: Brahmasta Adipradana) melalui [slack](https://kawalcovid19.slack.com/archives/CV49G29AM) untuk meningkatkan role anda menjadi relawan


Selanjutnya anda dapat mulai clone repo ini dan mulai berkontribusi berdasarkan issue yang open. Pastikan anda sudah mendapatkan akses pada project ini di Gitlab.

## Installation
Open Terminal/Console
* Clone Repo: `git clone git@gitlab.com:kawalcovid19/crowdsource-rs/crowdsource-rs.git`
* Enter to Project Directory: `cd crowdsource-rs`
* Install package dependencies: `npm install`
* Setup local certificates. Read [How to Create Your Own SSL Certificate Authority for Local HTTPS Development](https://medium.com/@hugomejia74/how-to-create-your-own-ssl-certificate-authority-for-local-https-development-3b97573c7bb5)
* Save local certificates (cert and key) to your home directory. I call it PATH_TO_CERTIFICATES
* Run `ng serve --ssl true --ssl-cert PATH_TO_CERTIFICATES/server.crt --ssl-key PATH_TO_CERTIFICATES/server.key`
* Access local development app at `https://localhost:4200/`

## Happy Coding
* pick assignment on opened issue;
* create branch, name it from feature or issue;
* write codes;
* commit your changes;
* push to your branch; and
* create merge request
